# UniParc

## uniparc_xref

The *uniparc_xref* table contans the following *types*:

| type                                  | count(*)  |
|---------------------------------------|-----------|
| EMBL                                  |  48083086 |
| EMBLWGS                               | 166017584 |
| EMBL_CON                              |  16902784 |
| EMBL_TPA                              |     23633 |
| EMBL_TSA                              |   1717304 |
| Ensembl                               |   3324714 |
| EnsemblBacteria                       | 111904338 |
| EnsemblFungi                          |   5174249 |
| EnsemblMetazoa                        |   1689393 |
| EnsemblPlants                         |   2311473 |
| EnsemblProtists                       |   1780928 |
| EPO                                   |   3301336 |
| FlyBase                               |     50153 |
| H-InvDB                               |   2607847 |
| IPI                                   |   1203226 |
| JPO                                   |   1891891 |
| KIPO                                  |    246459 |
| PATRIC                                |  80822029 |
| PDB                                   |    599408 |
| PIR                                   |    283420 |
| PIRARC                                |    342752 |
| PRF                                   |   1202937 |
| RefSeq                                | 113297614 |
| REMTREMBL                             |    143918 |
| SEED                                  |  34213143 |
| SGD                                   |      6237 |
| TAIR                                  |     40349 |
| TREMBLNEW                             |    642809 |
| TREMBL_VARSPLIC                       |      1051 |
| TROME                                 |   2057656 |
| UNIMES                                |   6028191 |
| UniProtKB/Swiss-Prot                  |    622222 | 622,222 rows
| UniProtKB/Swiss-Prot protein isoforms |     76188 | 75,592 rows, 617 co-occuring in TREMBL_VARSPLIC
| UniProtKB/TrEMBL                      | 125957105 |
| USPTO                                 |   3071524 |
| VEGA                                  |    248283 |
| WBParaSite                            |   2248912 |
| WormBase                              |     43337 |

38 rows in set (8 min 45.59 sec)

## uniparc_xref_prop

The *uniparc_xref_prop* table contins the following *types*:

| type                |
|---------------------|
| NCBI_GI             |
| NCBI_taxonomy_id    |
| protein_name        |
| gene_name           |
| chain               |
| UniProtKB_accession |
| proteome_id         |
| component           |

8 rows in set (47 min 26.54 sec)
