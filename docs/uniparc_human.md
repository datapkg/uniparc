# UniParc Human

## Mutation cross-referece

Mapping single AA mutation from RefSeq to UniProt (SwissProt canonical):

```sql
SELECT
-- RefSeq
m.index,
m.refseq_base_id,
m.refseq_mutation,
m.refseq_mutation_pos,
ux.active refseq_active,
ucuc.uniparc_sequence refseq_sequence,

-- UniProt
uxc.id uniprot_id,
FIND_IN_SET(refseq_mutation_pos, a2b) uniprot_mutation_pos,
uxc.active uniprot_active,
ucuc.uniparc_canonical_sequence uniprot_sequence

FROM staging.taipale_mmc3 m
JOIN uniparc_human.uniparc_xref ux ON (ux.type = 'RefSeq' AND ux.id = m.refseq_base_id)
JOIN uniparc_human.uniparc2uniparc_canonical_mapping ucuc USING (uniparc_id)
JOIN uniparc_human.uniparc_xref uxc ON (uxc.uniparc_id = ucuc.uniparc_canonical_id AND \
                                        uxc.type = 'UniProtKB/Swiss-Prot');
```
