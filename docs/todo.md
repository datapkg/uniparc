# TODO

- *uniparc2uniparc_canonical* -> *uniparc_xref* ('UniProtKB/Swiss-Prot') gives mappings to UniProts which are not active. Need to make sure that this is correct...
- Do not save `uniparc_xref`s which are not active (`active = 'N'`).
- Do not include sequences without a single active `uniparc_xref`.
- To speed up queries, each type should be given its own `xref` and `data` tables, e.g.::

    ncib_gi_xref_prop -> ncbi_gi
    ncbi_taxonomy_id_xref_prop -> ncbi_taxonomy_id
    ...

- Should end up with the following tables::

```raw
uniparc

uniparc_xref

uniparc_xref2ncbi_gi
uniparc_xref2ncbi_taxonomy_id
uniparc_xref2protein_name
uniparc_xref2gene_name
uniparc_xref2chain
uniparc_xref2uniprot_kb_accession
uniparc_xref2proteome_id
uniparc_xref2component

ncbi_gi
ncbi_taxonomy_id
protein_name
gene_name
chain
uniprot_kb_accession
proteome_id
component
```
