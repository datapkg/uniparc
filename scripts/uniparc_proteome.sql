
# Create a temporary table with human `uniparc_xref_id`s
DROP TABLE IF EXISTS `_uniparc_xref_id_proteome`;

CREATE TABLE `_uniparc_xref_id_proteome` AS
SELECT uniparc_xref_id
FROM uniparc.uniparc_xref_prop
WHERE type = 'NCBI_taxonomy_id' and value = '{ncbi_taxonomy_id}';

ALTER TABLE _uniparc_xref_id_proteome ADD PRIMARY KEY (uniparc_xref_id);



# Create temporary table with human `uniparc_id`s
DROP TABLE IF EXISTS `_uniparc_id_proteome`;

CREATE TABLE `_uniparc_id_proteome` AS
SELECT DISTINCT(uniparc_id)
FROM uniparc.uniparc_xref
WHERE uniparc_xref_id in (select uniparc_xref_id from _uniparc_xref_id_proteome);

ALTER TABLE _uniparc_id_proteome ADD PRIMARY KEY (uniparc_id);



# uniparc_sequence
DROP TABLE IF EXISTS `uniparc_sequence`;

CREATE TABLE `uniparc_sequence` LIKE `uniparc`.`uniparc_sequence`;

INSERT INTO `uniparc_sequence`
SELECT *
FROM `uniparc`.`uniparc_sequence`
WHERE uniparc_id IN (SELECT uniparc_id FROM _uniparc_id_proteome);



# uniparc_xref
DROP TABLE IF EXISTS `uniparc_xref`;

CREATE TABLE `uniparc_xref` LIKE `uniparc`.`uniparc_xref`;
ALTER TABLE `uniparc_xref` ADD COLUMN id_versioned VARCHAR(255);

INSERT INTO `uniparc_xref`
SELECT *, CONCAT(id, '.', version_i)
FROM `uniparc`.`uniparc_xref`
WHERE uniparc_xref_id IN (SELECT uniparc_xref_id FROM _uniparc_xref_id_proteome);

CREATE INDEX p ON `uniparc_xref` (uniparc_xref_id, id_versioned);
CREATE INDEX q ON `uniparc_xref` (id_versioned, uniparc_xref_id);



# uniparc_xref_prop
DROP TABLE IF EXISTS `uniparc_xref_prop`;

CREATE TABLE `uniparc_xref_prop` LIKE `uniparc`.`uniparc_xref_prop`;

INSERT INTO `uniparc_xref_prop`
SELECT *
FROM `uniparc`.`uniparc_xref_prop`
WHERE uniparc_xref_id IN (SELECT uniparc_xref_id FROM _uniparc_xref_id_proteome);



# Cleanup
DROP TABLE IF EXISTS `_uniparc_id_proteome`;
DROP TABLE IF EXISTS `_uniparc_xref_id_proteome`;
