# 0
CREATE DATABASE IF NOT EXISTS `uniparc_active`;
USE `uniparc_active`;


# 1
CREATE TABLE `uniparc_active`.`uniparc` (
  `uniparc_id` varchar(32) NOT NULL,
  `length` int(11) DEFAULT NULL,
  `checksum` varchar(32) DEFAULT NULL,
  `sequence` mediumtext,
  `uniprotkb_exclusion` varchar(32),
  PRIMARY KEY (`uniparc_id`),
  UNIQUE KEY `A` (`uniparc_id`, `length`, `checksum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


# 2
BEGIN;

ALTER TABLE `uniparc_active`.`uniparc` DISABLE KEYS;

INSERT INTO `uniparc_active`.`uniparc`
SELECT
`uniparc_id`,
`length`,
`checksum`,
`sequence`,
`uniprotkb_exclusion`
FROM `uniparc`.`uniparc`
JOIN `uniparc`.`uniparc_sequence` using (uniparc_id);

ALTER TABLE `uniparc_active`.`uniparc` ENABLE KEYS;

COMMIT;


# 3
CREATE TABLE `uniparc_active`.`uniparc_xref` LIKE `uniparc`.`uniparc_xref`;


# 4
BEGIN;

ALTER TABLE `uniparc_active`.`uniparc_xref` DISABLE KEYS;

INSERT INTO `uniparc_active`.`uniparc_xref`
SELECT *
FROM uniparc.uniparc_xref
WHERE active = 'Y';

ALTER TABLE `uniparc_active`.`uniparc_xref` ENABLE KEYS;

COMMIT;


# 5
CREATE TABLE `uniparc_active`.`uniparc_xref_prop` LIKE `uniparc`.`uniparc_xref_prop`;


# 6
BEGIN;

ALTER TABLE `uniparc_active`.`uniparc_xref_prop` DISABLE KEYS;

INSERT INTO `uniparc_active`.`uniparc_xref_prop`
SELECT uxp.*
FROM `uniparc`.`uniparc_xref_prop` uxp
JOIN `uniparc_active`.`uniparc_xref`;

ALTER TABLE `uniparc_active`.`uniparc_xref_prop` ENABLE KEYS;

COMMIT;
