DELIMITER $$
CREATE FUNCTION `CONVERT_POSITION`(
  x TEXT,
  delim VARCHAR(12),
  idx INT
) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
BEGIN
DECLARE pos_new varchar(255);
DECLARE pos_new_int int;
SET pos_new =
	REPLACE(
		SUBSTRING(
			SUBSTRING_INDEX(x, delim, idx),
			LENGTH(SUBSTRING_INDEX(x, delim, idx - 1)) + 1),
		delim,
		'');
SET pos_new_int =
	CASE
		WHEN x is null THEN idx
		WHEN pos_new = '' THEN 0
		ELSE CONVERT(pos_new, unsigned integer)
	END;
RETURN pos_new_int;
END$$
DELIMITER ;
