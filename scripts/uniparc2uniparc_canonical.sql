
# gene_name
DROP TABLE IF EXISTS gene_name;

CREATE TABLE gene_name (
    gene_name_id INT(11) AUTO_INCREMENT PRIMARY KEY,
    gene_name VARCHAR(255) NOT NULL
) ENGINE = MYISAM;

INSERT INTO gene_name (gene_name)
SELECT DISTINCT `value`
FROM uniparc_xref_prop
WHERE `type` = 'gene_name';

ALTER TABLE gene_name
ADD UNIQUE INDEX A (gene_name_id, gene_name),
ADD UNIQUE INDEX B (gene_name, gene_name_id);



# uniparc_xref2gene_name
DROP TABLE IF EXISTS uniparc_xref2gene_name;

CREATE TABLE uniparc_xref2gene_name AS
SELECT DISTINCT ux.uniparc_xref_id, gn.gene_name_id
FROM uniparc_xref ux
JOIN uniparc_xref_prop uxp ON (uxp.type = 'gene_name' and uxp.uniparc_xref_id = ux.uniparc_xref_id)
JOIN gene_name gn ON (gn.gene_name = uxp.value);

ALTER TABLE uniparc_xref2gene_name
ADD COLUMN idx INT(11) AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE INDEX A (uniparc_xref_id, gene_name_id),
ADD UNIQUE INDEX B (gene_name_id, uniparc_xref_id),
ENGINE = MYISAM;



# uniparc2uniparc_canonical
DROP TABLE IF EXISTS uniparc2uniparc_canonical;

CREATE TABLE uniparc2uniparc_canonical AS
SELECT
us.uniparc_id,
us2.uniparc_id uniparc_id_canonical,
us.sequence uniparc_sequence,
us2.sequence uniparc_sequence_canonical
FROM uniparc_sequence us
JOIN uniparc_xref ux ON (
    ux.active = 'Y'
    AND (
        ux.type = 'Ensembl' or
        ux.type = 'RefSeq' or
        ux.type = 'UniProtKB/Swiss-Prot' or
        ux.type = 'UniProtKB/Swiss-Prot protein isoforms' or
        ux.type = 'UniProtKB/TrEMBL'
    ) AND ux.uniparc_id = us.uniparc_id)
JOIN uniparc_xref2gene_name uxg ON (uxg.uniparc_xref_id = ux.uniparc_xref_id)
JOIN uniparc_xref2gene_name uxg2 ON (uxg2.gene_name_id = uxg.gene_name_id)
JOIN uniparc_xref ux2 ON (
    ux2.active = 'Y'
    AND ux2.type = 'UniProtKB/Swiss-Prot'
    AND ux2.uniparc_xref_id = uxg2.uniparc_xref_id)
JOIN uniparc_sequence us2 ON (us2.uniparc_id = ux2.uniparc_id)
GROUP BY us.uniparc_id, us2.uniparc_id;

ALTER TABLE uniparc2uniparc_canonical
ADD COLUMN idx INT(11) AUTO_INCREMENT PRIMARY KEY FIRST,
ADD UNIQUE INDEX A (uniparc_id, uniparc_id_canonical),
ADD UNIQUE INDEX B (uniparc_id_canonical, uniparc_id),
ENGINE = MYISAM;
