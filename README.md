# uniparc

[![docs](https://img.shields.io/badge/docs-v0.2-blue.svg)](https://datapkg.gitlab.io/uniparc/)
[![build status](https://gitlab.com /datapkg/uniparc/badges/master/build.svg)](https://gitlab.com /datapkg/uniparc/commits/master/)

Convert UniParc XML file (`uniparc_all.xml.gz`) into Parquet tables.

## Notebooks

#### [01-download.ipynb](notebooks/01-download.ipynb)

- Download UniParc XML file.

#### [02-uniparc_xml_parser.ipynb](notebooks/02-uniparc_xml_parser.ipynb)

- Process the UniParc XML file into CSV files.

#### [03-to_parquet.ipynb](notebooks/03-to_parquet.ipynb)

- Convert generated XML files into the Apache Parquet file format.

## Scripts

Deploy generated files to our data warehouse:

```bash
./gitlab-ci.sh deploy
```
